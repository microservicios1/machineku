<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Maquinas Virtuales</title>
  <style>
    body
    {
      background-image: url(all-of-those-images/interf/logoazul.png);
      background-repeat: no-repeat;
      background-size: 150px 45px;
      background-position: 6% 25px;
    }
    table
    {
      border: 1px solid black;
    }
    th
    {
      font-size: 14px;
      border: 1px solid black;
      text-align: center;
    }
    td
    {
      text-align: center;
      font-size: 11px;
      border: 1px solid black;
    }
    .shad
    {
      font-size: 20px;
      font-weight: bold;
      border: 1px flat #000033;
    }
    .evilbtn
    {
      display: inline;
      background-color: #F7DC6F;
      border:  double #FCF3CF;
      font-size: 15px;
      font-weight: bold;
      float: right;
      margin-right: 5%;
      cursor: default;
      border-radius: 50%;
      height: 60px;
    }
    .containerOfRod
    {
      padding: 4px 4px;
      font-size: 14px;
      border:10px groove #616161;
      border-radius: 10px;
      position:absolute;
    }
    button,input[type=submit],input[type=reset]
    {
      background-color: #D6EAF8;
      padding: 4px 4px;
      border: outset #ABB2B9;
      cursor: pointer;
      font-size: 15px;
      font-weight: bold;
      box-shadow: 2px 3px 10px #000033;
    }
  </style>
  <?php
    include 'dbc.php';
    $conn = mysqli_connect($host,$user,$pass,$db);
  ?>
  <script>
    function isip(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57)&&(ch!=46))
        return false;
      return true;
    }
    function isdate(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57)&&(ch!=45))
        return false;
      return true;
    }
    function isSolid(evt)
    {
      var ch = (evt.which) ? evt.which : event.keyCode
      if (ch > 31 &&(ch<48||ch>57))
        return false;
      return true;
    }
  </script>
</head>
<body>
  <form method="post"  action="moveMachine.php" class="containerOfRod"> <br>
    <br>
    <div class="shad" align="center">
      <?php for($i=0;$i<8;$i++) echo "&nbsp;"; ?><input type="submit" value="Modificar datos de maquinas"> &nbsp;
    </div>
    <br><br>
    <?php
      $sql="select DIP,Name,IDef,Sta,SVCPU,SRAM,SSto,SSO,SDB,DVCPU,DRAM,DSto,DSO,DDB,Arre,Typ,no from maquinas where id=".$_POST['no']." order by Arre";
      $re = mysqli_query($conn,$sql);
      if(!$re)
        echo "Conexion con BD fallida";
      else
      {
        $i=0;
        $act=0;
        while($row = mysqli_fetch_array($re))
        {
          if($act!=$row['Arre'])
          {
            if($act!=0)
            {
              if($last[0]=="Cluster")
                giveshared($last[1]);
              if($last[0]=="Standalone")
                echo "</table><br><br>";
            }
            $act=$row['Arre'];
            gimmetitles($row['Typ']);
          }
          givesomemachine($row,$i);
          $last[0]=$row['Typ'];
          $last[1]=$row['Arre'];
          $i++;
        }
        if($last[0]=="Cluster")
          giveshared($last[1]);
        if($last[0]=="Standalone")
          echo "</table><br><br>";
        $i=0;
        $sql="select DIP,FESO,FEDB,APP,ATP,F60,PruR,FUF from maquinas where id=".$_POST['no']." order by Arre";
        $re = mysqli_query($conn,$sql);
        if(!$re)
          echo "Conexion con BD fallida";
        else
        {
          gimmedates();
          while($row = mysqli_fetch_array($re))
          {
            givesomerealdates($row,$i,$_POST['no']);
            $i++;
          }
          echo "</table><br>";
        }
      }
    ?>
    <?php
      echo "<input type=\"hidden\" name=\"rrr\" value=\"".$_POST['no']."\" >";
      function giveshared($di)
      {
        include 'dbc.php';
        $conn = mysqli_connect($host,$user,$pass,$db);
        $sql="select CoDi,SDQ,EDQ from ShaDQ where id=".$_POST['no']." and Arre=".$di;
        $gimmemethashared=mysqli_query($conn,$sql);
        $thashared=mysqli_fetch_array($gimmemethashared);
        echo "</table><br>Disco compartido Solicitado <input type=\"text\" name=\"SDQ".$di."\" value=\"".$thashared['SDQ']."\" ";
        if($thashared['SDQ']==""||$thashared['SDQ']==0)
          echo " style=\"background:#85807d\"";
        echo "autocomplete=\"off\" readonly=\"readonly\">";
        echo " &nbsp; Disco compartido Entregado <input type=\"number\" name=\"EDQ".$di."\"";
        if($thashared['EDQ']==""||$thashared['EDQ']==0)
          echo " style=\"background:#85807d\"";
        echo " value=\"".$thashared['EDQ']."\" autocomplete=\"off\" >";
        echo "&nbsp;&nbsp; Comentarios <input type=\"text\" name=\"CoDi".$di."\"";
        if($thashared['CoDi']=="")
          echo " style=\"background:#85807d\"";
        echo " value=\"".$thashared['CoDi']."\" autocomplete=\"off\" ><br><br>";
      }
      function gimmetitles($tt)
      {
        ?>
        <table >
          <tr>
            <th width="20">IP: (modificable)</th>
            <th width="20">Nombre:</th>
            <th width="20">Infra def:</th>
            <th width="20">Status:</th>
            <th width="20">vCPUs:</th>
            <th width="20">Memoria <br />RAM(gb):</th>
            <th width="20">Disco <br />(gb):</th>
            <th width="20">SO:</th>
            <?php if($tt=="Cluster") echo "<th  width=\"20\">Base De<br /> Datos:</th>" ?>
            <th width="20">vCPUs <br />(Entr):</th>
            <th width="20">Memoria RAM<br />(gb)<br />(Entr):</th>
            <th width="20">Disco (gb)<br />(Entr):</th>
            <th width="20">SO (Entrgado):</th>
            <?php if($tt=="Cluster") echo "<th  width=\"20\">Base De Datos: <br />(Entrgada)</th>" ?>
          </tr>
        <?php
      }
      function givesomemachine($r,$i)
      {
        include 'dbc.php';
        $conn = mysqli_connect($host,$user,$pass,$db);
        ?>
        <tr>
          <td><input type="text" size="10" name=<?php echo "\"DIP".$i."\""; ?> value=<?php echo "\"".$r['DIP']."\""; if($r['DIP']=="") echo " style=\"background:#85807d\"";?> autocomplete="off" onkeypress="return isip(event)"   maxlength="16" oninput="if(this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"></td>
          <td><input type="text" size="10" name=<?php echo "\"Name".$i."\"";  echo "value=\"".$r['Name']."\""; if($r['Name']=="") echo " style=\"background:#85807d\"";?> autocomplete="off" ></td>
          <input type="hidden" name=<?php echo "\"no".$i."\"";  echo "value=\"".$r['no']."\"";?>>
          <td>
            <select name=<?php echo "\"IDef".$i."\""; if($r['IDef']=="") echo " style=\"background:#85807d\"";?> style="width:100px" autocomplete="off">
              <option value=""></option>
              <?php
                $re2 = mysqli_query($conn,"select nombre from infdef");
                if(! $re2)
                  echo "<option value=\"Pendiente\">Pendiente</option> ";
                else
                {
                  while($row2 = mysqli_fetch_array($re2))
                  {
                    $o ="<option ";
                    if($r['IDef'] == $row2['nombre'])
                      $o .= " selected ";
                    $o .= "value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
                    echo $o;
                  }
                }
              ?>
            </select>
          </td>
          <td>
            <select name=<?php echo "\"Sta".$i."\""; if($r['Sta']=="") echo " style=\"background:#85807d\"";?> style="width:100px" autocomplete="off">
              <option value=""></option>
              <option value="Pendiente" <?php if($r['Sta'] == "Pendiente") echo " selected ";?> >Pendiente</option>
              <option value="COYM" <?php if($r['Sta'] == "COYM") echo " selected ";?> >COYM</option>
              <option value="Pendiente sin recursos" <?php if($r['Sta'] == "Pendiente sin recursos") echo " selected ";?> >Pendiente sin recursos</option>
              <option value="Entregado por COYM" <?php if($r['Sta'] == "Entregado por COYM") echo " selected ";?> >Entregado por COYM</option>
              <option value="Entregado BD" <?php if($r['Sta'] == "Entregado BD") echo " selected ";?> >Entregado BD</option>
              <option value="Entregado SO" <?php if($r['Sta'] == "Entregado SO") echo " selected ";?> >Entregado SO</option>
              <option value="Entregado" <?php if($r['Sta'] == "Entregado") echo " selected ";?> >Entregado</option>
              <option value="Entregado a usuario" <?php if($r['Sta'] == "Entregado a usuario") echo " selected ";?> >Entregado a usuario</option>
              <option value="Eliminada" <?php if($r['Sta'] == "Eliminada") echo " selected ";?> >Eliminada</option>
            </select>
          </td>
          <td><input type="text" size="3" name=<?php echo "\"SVCPU".$i."\""; if($r['SVCPU']==""||$r['SVCPU']==0) echo " style=\"background:#85807d\"";?> autocomplete="off" oninput="if(this.value.length > 4) this.value = this.value.slice(0, this.maxLength);" onkeypress="return isSolid(event)" <?php echo "value=\"".$r['SVCPU']."\""; ?>></td>
          <td><input type="text" size="3" name=<?php echo "\"SRAM".$i."\""; if($r['SRAM']==""||$r['SRAM']==0) echo " style=\"background:#85807d\"";?> autocomplete="off" oninput="if(this.value.length > 4) this.value = this.value.slice(0, this.maxLength);" onkeypress="return isSolid(event)" <?php echo "value=\"".$r['SRAM']."\""; ?>></td>
          <td><input type="text" size="3" name=<?php echo "\"SSto".$i."\""; if($r['SSto']==""||$r['SSto']==0) echo " style=\"background:#85807d\"";?> autocomplete="off" oninput="if(this.value.length > 6) this.value = this.value.slice(0, this.maxLength);" onkeypress="return isSolid(event)" <?php echo "value=\"".$r['SSto']."\""; ?>></td>
          <td>
            <select name=<?php echo "\"SSO".$i."\""; if($r['SSO']=="") echo " style=\"background:#85807d\"";?> style="width:100px" autocomplete="off">
              <option value=""></option>
              <?php
                $re2 = mysqli_query($conn,"select nombre from SO");
                if(! $re2)
                  echo "<option value=\"Pendiente\">Pendiente</option> ";
                else
                {
                  while($row2 = mysqli_fetch_array($re2))
                  {
                    $o ="<option ";
                    if($r['SSO'] == $row2['nombre'])
                      $o .= " selected ";
                    $o .= "value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
                    echo $o;
                  }
                }
              ?>
            </select>
          </td>
          <?php
            if($r['Typ']=="Cluster")
            { ?>
            <td>
            <select name=<?php echo "\"SDB".$i."\""; if($r['SDB']=="") echo " style=\"background:#85807d\"";?> style="width:80px" autocomplete="off">
              <option value="N/A">N/A</option>
              <?php
                $re2 = mysqli_query($conn,"select nombre from datab");
                if(! $re2)
                  echo "<option value=\"Pendiente\">Pendiente</option> ";
                else
                {
                  while($row2 = mysqli_fetch_array($re2))
                  {
                    $o ="<option ";
                    if($r['SDB'] == $row2['nombre'])
                      $o .= " selected ";
                    $o .= "value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
                    echo $o;
                  }
                }
              ?>
            </select>
            </td>
            <?php
            }
          ?>
          <td><input type="text" size="3" name=<?php echo "\"DVCPU".$i."\""; if($r['DVCPU']==""||$r['DVCPU']==0) echo " style=\"background:#85807d\"";?> autocomplete="off" oninput="if(this.value.length > 4) this.value = this.value.slice(0, this.maxLength);" onkeypress="return isSolid(event)" <?php echo "value=\"".$r['DVCPU']."\""; ?>></td>
          <td><input type="text" size="3" name=<?php echo "\"DRAM".$i."\""; if($r['DRAM']==""||$r['DRAM']==0) echo " style=\"background:#85807d\"";?> autocomplete="off" oninput="if(this.value.length > 4) this.value = this.value.slice(0, this.maxLength);" onkeypress="return isSolid(event)" <?php echo "value=\"".$r['DRAM']."\""; ?>></td>
          <td><input type="text" size="3" name=<?php echo "\"DSto".$i."\""; if($r['DSto']==""||$r['DSto']==0) echo " style=\"background:#85807d\"";?> autocomplete="off" oninput="if(this.value.length > 6) this.value = this.value.slice(0, this.maxLength);" onkeypress="return isSolid(event)" <?php echo "value=\"".$r['DSto']."\""; ?>></td>
          <td>
            <select name=<?php echo "\"DSO".$i."\""; if($r['DSO']=="") echo " style=\"background:#85807d\"";?> style="width:100px" autocomplete="off">
              <option value=""></option>
              <?php
                $re2 = mysqli_query($conn,"select nombre from SO");
                if(! $re2)
                  echo "<option value=\"Pendiente\">Pendiente</option> ";
                else
                {
                  while($row2 = mysqli_fetch_array($re2))
                  {
                    $o ="<option ";
                    if($r['DSO'] == $row2['nombre'])
                      $o .= " selected ";
                    $o .= "value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
                    echo $o;
                  }
                }
              ?>
            </select>
          </td>
          <?php
            if($r['Typ']=="Cluster")
            { ?>
            <td>
            <select name=<?php echo "\"DDB".$i."\""; if($r['DDB']=="") echo " style=\"background:#85807d\"";?> style="width:80px" autocomplete="off">
              <option value="N/A">N/A</option>
              <?php
                $re2 = mysqli_query($conn,"select nombre from datab");
                if(! $re2)
                  echo "<option value=\"Pendiente\">Pendiente</option> ";
                else
                {
                  while($row2 = mysqli_fetch_array($re2))
                  {
                    $o ="<option ";
                    if($r['DDB'] == $row2['nombre'])
                      $o .= " selected ";
                    $o .= "value=\"".$row2['nombre']."\">".$row2['nombre']."</option>";
                    echo $o;
                  }
                }
              ?>
            </select>
            </td>
            <?php
            }
          ?>
        </tr>
        <?php
      }
      function gimmedates()
      {
        ?>
        <table>
          <tr>
            <th width="20">IP: (no modificable)</th>
            <th width="20">F. Entrega SO:</th>
            <th width="20">F. Entrega DB:</th>
            <th width="20">F. APP:</th>
            <th width="20">F. ATP:</th>
            <th width="20">F. F60:</th>
            <th width="20">P. Respaldo:</th>
            <th width="20">F. Entrega <br />a User:</th>
            <th rowspan=<?php $h=$i+1; echo "\"$h\"";?>><button type="button" class="evilbtn">Tecnologias Cloud</button></th>
          </tr>
        <?php
      }
      function givesomerealdates($r,$i,$chose)
      {
        include 'dbc.php';
        $conn = mysqli_connect($host,$user,$pass,$db);
        ?>
          <tr>
            <td><input type="text" size="10" value=<?php echo "\"".$r['DIP']."\""; if($r['DIP']=="") echo " style=\"background-color:#85807d\"";?> readonly="readonly"></td>
            <td><input type="date" name=<?php echo "\"FESO".$i."\" value=\"".$r['FESO']."\" style=\"width:130px;";if($r['FESO']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
            <td><input type="date" name=<?php echo "\"FEDB".$i."\" value=\"".$r['FEDB']."\" style=\"width:130px;";if($r['FEDB']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
            <td><input type="date" name=<?php echo "\"APP".$i."\" value=\"".$r['APP']."\" style=\"width:130px;";if($r['APP']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
            <td><input type="date" name=<?php echo "\"ATP".$i."\" value=\"".$r['ATP']."\" style=\"width:130px;";if($r['ATP']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
            <td><input type="date" name=<?php echo "\"F60".$i."\" value=\"".$r['F60']."\" style=\"width:130px;";if($r['F60']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
            <td><input type="date" name=<?php echo "\"PruR".$i."\" value=\"".$r['PruR']."\" style=\"width:130px;";if($r['PruR']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
            <td><input type="date" name=<?php echo "\"FUF".$i."\" value=\"".$r['FUF']."\" style=\"width:130px;";if($r['FUF']=="") echo "background-color:#85807d;"; echo "\"";?>></td>
            <td>
              <?php
                echo "<input type=\"button\" onclick=\"window.open('timemachine.php?no=".$chose."&mac=".$i."','','menubar=0,titlebar=0,width=350,height=400,resizable=0,left=40px,top=50px')\" value=\"Calendario\" />";
              ?>
            </td>
          </tr>
        <?php
      }
    ?>
  </form>
</body>
</html>