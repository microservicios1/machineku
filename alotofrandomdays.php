<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Calendario 1</title>
  <style>
  	#calendar 
    {
			font-family:Arial;
			font-size:12px;
		}
    #calendar caption 
    {
			text-align:left;
			padding:5px 10px;
			background-color:#003366;
			color:#fff;
			font-weight:bold;
		}
    #calendar .feso
    {
			background-color:#c97171;
		}
    #calendar .fedb 
    {
			background-color:#f55e2c;
		}
    #calendar .app 
    {
			background-color:#c7a75d;
		}
    #calendar .atp 
    {
			background-color:#e7ed32;
		}
    #calendar .f60
    {
			background-color:#b8db67;
		}
    #calendar .prur 
    {
			background-color:#67db6f;
		}
    #calendar .fuf
    {
			background-color:#708a6b;
		}
    body
    {
      background-image: url(all-of-those-images/interf/logoazul.png);
      background-repeat: no-repeat;
      background-size: 120px 35px;
      background-position: 20px 25px;
    }
    #calendar caption div:nth-child(1) {float:left;}
		#calendar caption div:nth-child(2) {float:right;}
		#calendar caption div:nth-child(2) a {cursor:pointer;}
    table
    {
      border: 1px solid black;  
    }
    th
    {
      background-color:#006699;
			color:#fff;
			width:40px;
    }
    td
    {
      text-align:right;
			padding:2px 5px;
			background-color:silver;
    }
    .shad
    {
      font-size: 20px;
      font-weight: bold;
      border: 1px flat #000033;
    }
    .evilbtn
    {
      display: inline;
      background-color: #F7DC6F;
      border:  double #FCF3CF;
      font-size: 15px;
      font-weight: bold;
      float: right;
      margin-right: 5%;
      cursor: default;
      border-radius: 50%;
      height: 60px;
    }
    .containerOfRod
    {
      padding: 4px 4px;
      font-size: 14px;
      border:10px groove #616161;
      border-radius: 10px; 
      position:absolute;
    }
    button,input[type=submit],input[type=reset]
    {
      background-color: #D6EAF8;
      padding: 4px 4px;
      border: outset #ABB2B9;
      cursor: pointer;
      font-size: 15px;
      font-weight: bold;
      box-shadow: 2px 3px 10px #000033;
    }
  </style>
  <?php 
    include 'dbc.php'; 
    $conn = mysqli_connect($host,$user,$pass,$db);
    $sql="select no from maquinas where id=".$_POST['no']." order by Arre";
    $re = mysqli_query($conn,$sql);
    if(!$re)
      echo "Conexion con BD fallida";
    else
    {
      $i=0;
      while($row = mysqli_fetch_array($re))
      {
        if($i==$_POST['mac'])
        {
          $sql="select FESO,FEDB,APP,ATP,F60,PruR,FUF from maquinas where no=".$row['no'];
          $r = mysqli_query($conn,$sql);
          if(!$r)
            echo "No se lograron recuperar fechas";
          else
          {
            $thosedays = mysqli_fetch_array($r);
            $c=array('FESO','FEDB','APP','ATP','F60','PruR','FUF');
            $h=0;
            echo "<script type='text/javascript'>";
            for($j=0;$j<7;$j++)
              if($thosedays[$j]!="")
              {
                $names[$h]=$c[$j];
                $day[$h]=new DateTime($thosedays[$j]);
                $some=explode("-",$thosedays[$j]);
                $days[$h]= $some[2];
                $months[$h]= $some[1];
                $years[$h]= $some[0]; 
                $h++;
              }
            for($j=0;$j<sizeof($days);$j++)
              for($k=0;$k<sizeof($day);$k++)
                if($day[$j]<$day[$k])
                {
                  $temp=$day[$j];
                  $day[$j]=$day[$k];
                  $day[$k]=$temp;
                  $temp=$names[$j];
                  $names[$j]=$names[$k];
                  $names[$k]=$temp;
                  $temp=$days[$j];
                  $days[$j]=$days[$k];
                  $days[$k]=$temp;
                  $temp=$months[$j];
                  $months[$j]=$months[$k];
                  $months[$k]=$temp;
                  $temp=$years[$j];
                  $years[$j]=$years[$k];
                  $years[$k]=$temp;
                }
                
            $chain = " var days = [";
            for($k=0;$k<sizeof($days);$k++)
            {
              if($k>0)
                $chain .= ",";
              $chain .= $days[$k];
            }  
            $chain .= "]; ";
            $chain .= "var months = [";
            for($k=0;$k<sizeof($months);$k++)
            {
              if($k>0)
                $chain .= ",";
              $chain .= $months[$k];
            }  
            $chain .= "]; var years = [";
            for($k=0;$k<sizeof($years);$k++)
            {
              if($k>0)
                $chain .= ",";
              $chain .= $years[$k];
            }  
            $chain .= "]; var flags=[";
            for($k=0;$k<sizeof($names);$k++)
            {
              if($k>0)
                $chain .= ",";
              if($k==(sizeof($names)-1))
                $chain .="1";
              else
                $chain .= "0";
            }  
            $chain .= "]; var thoseclases=[";
            for($k=0;$k<sizeof($names);$k++)
            {
              if($k>0)
                $chain .= ",";
              $chain .= "'".strtolower($names[$k])."'";
            }
            $chain .= "];";
            echo $chain." </script>";
          } 
        }
        $i++;
      }
    }
  ?>
</head>
<body>
  <div class="containerOfRod">
    <h3 align="center">&nbsp;&nbsp;&nbsp;Calendario</h3>
    <table id="calendar" align="center">
      <caption></caption>
      <thead>
		    <tr>
			    <th>Lun</th><th>Mar</th><th>Mie</th><th>Jue</th><th>Vie</th><th>Sab</th><th>Dom</th>
	    	</tr>
    	</thead>
      <tbody>
      </tbody>
    </table>
    <br>
    <button type="button" style="display: inline; background-color:#c97171;font-size:11px;cursor:default;height:20px;">entrega SO</button>
    <button type="button" style="display: inline; background-color:#f55e2c;font-size:11px;cursor:default;height:20px;">Entrega DB</button><br>
    <button type="button" style="display: inline; background-color:#c7a75d;font-size:11px;cursor:default;height:20px;">APP</button>
    <button type="button" style="display: inline; background-color:#e7ed32;font-size:11px;cursor:default;height:20px;">ATP</button><br>
    <button type="button" style="display: inline; background-color:#b8db67;font-size:11px;cursor:default;height:20px;">F60</button>
    <button type="button" style="display: inline; background-color:#67db6f;font-size:11px;cursor:default;height:20px;">Respaldo</button><br>
    <button type="button" style="display: inline; background-color:#708a6b;font-size:11px;cursor:default;height:20px;">Usuario</button>
    <button type="button" class="evilbtn">Tecnologias Cloud</button>
  </div>
</body>
<script>
  var actual=new Date();
  function mostrarCalendario(year,month)
  {
    var istodaypainted=0;
    var p=0,w=0,t=0;
    var now=new Date(year,month-1,1);
	  var last=new Date(year,month,0);
	  var primerDiaSemana=(now.getDay()==0)?7:now.getDay();
	  var ultimoDiaMes=last.getDate();
	  var dia=0;
	  var resultado="<tr bgcolor='silver'>";
	  var diaActual=0;
	  console.log(ultimoDiaMes);
  	var last_cell=primerDiaSemana+ultimoDiaMes;
   	for(var i=1;i<=42;i++)
	  {
		  if(i==primerDiaSemana)
		    dia=1;
			if(i<primerDiaSemana || i>=last_cell)
		  	resultado+="<td>&nbsp;</td>";
		  else
      {
        //FESO,FEDB,APP,ATP,F60,PruR,FUF
        if(year<=actual.getFullYear())
        {
          p=1;
          if(year==actual.getFullYear()&&month>=actual.getMonth()+1)
          {
            p=0;
            if(month==actual.getMonth()+1&&dia<actual.getDate())
              p=1;
          }
        }
        else
          p=0;
        if(year>=years[0])
        {
          w=1;
          if(year==years[0]&&month<=months[0])
          {
            w=0;
            if(month==months[0]&&dia>=days[0])
              w=1;
          }
        }
        else
          w=0;
        if(p==1&&w==1)
          for(h=0;h<thoseclases.length;h++)
			    {
            if(year>=years[h])
            {
              t=1;
              if(year==years[h]&&month<=months[h])
              {
                t=0;
                if(month==months[h]&&dia>=days[h])
                  t=1;
              }
            }
            else
              t=0;
            if(t==1)
            {
              for(j=0;j<thoseclases.length;j++)
                flags[j]=0;
              flags[h]=1;
            }
          }
        istodaypainted=0;
        for(j=0;j<thoseclases.length;j++)
          if(flags[j]==1&&istodaypainted==0&&p==1&&w==1)
          {
            resultado+="<td class='"+thoseclases[j]+"'>"+dia+"</td>";
            istodaypainted=1;
          }
        if(istodaypainted==0)
          resultado+="<td>"+dia+"</td>";
        dia++;
		  }
		  if(i%7==0)
		  {
			  if(dia>ultimoDiaMes)
				  break;
			  resultado+="</tr><tr>\n";
		  }
	  }
	  resultado+="</tr>";
    var meses=Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
   	nextMonth=month+1;
	  nextYear=year;
	  if(month+1>12)
	  {
		  nextMonth=1;
		  nextYear=year+1;
	  }
    prevMonth=month-1;
	  prevYear=year;
	  if(month-1<1)
	  {
		  prevMonth=12;
		  prevYear=year-1;
	  }
  	document.getElementById("calendar").getElementsByTagName("caption")[0].innerHTML="<div>"+meses[month-1]+" / "+year+"</div><div><a onclick='mostrarCalendario("+prevYear+","+prevMonth+")'>&lt;</a> <a onclick='mostrarCalendario("+nextYear+","+nextMonth+")'>&gt;</a></div>";
	  document.getElementById("calendar").getElementsByTagName("tbody")[0].innerHTML=resultado;
  }
  mostrarCalendario(actual.getFullYear(),actual.getMonth()+1);
</script>
</html>